---
title: Protecting Masks
author: Laura Freixas
category: Health
layout: post
---

# The Shape of Necessity

### Introduction to Project by Tomas
The Master in Design for Emergent Futures is proposing a revision of the publication made by the Cuban government during the 1990's, when the island was going through the special period after the fall of the Iron Curtain. MDEF's tutors and students will work in a new repository and publication, understanding that the world is now in a new "special period", after the release of the COVID-19.

Knowing the limitations of mobility that the world is experiencing, there is a dramatic change in logistics and supply chains, as well as new ways to access knowledge. Right now, we are more connected than ever and we are more isolated than ever at the same time. The current global pandemic is opening up opportunities for hyperlocal interventions that can help people to live confinement in much better conditions while increasing long term resilience.

The exercise will consist of mapping and documenting existing practices that are happening now at the domestic scale in the world, and which people are using to solve local needs, by following open source projects or existing documentation. The work will be organized following the categories of the book "Con Nuestros Propios Esfuerzos".

These categories will be assigned to students by MDEF tutors, and they will serve as a starting point to organize their work for the week. A second part of the exercise will consist of imagining near-future scenarios, starting from January 2021, and proposing the evolution of these categories and projects in the context of the Post-Corona society. We want our students to identify not only the current situation of confinement but also being able to anticipate the world that could emerge out of this forced transition. Starting from January 2021, we want our students to make an effort to identify what these collective practices or this domestic practices could look like in six or nine months.

Ernesto Oroza, who is going to be a collaborator in this week's exercise, will participate in the collective efforts by inspiring students in how to curate and identify which kind of solutions or which kind of recipes are being developed and shared around the world as never before.

Objectives:

- to document do-it-yourself practices that are emerging at the domestic space from people that are solving basic needs related with food, fixtures of electronics, clothing, or fixing anything that is in their homes

- to curate together with MDEF tutors a collection of solutions and projects that are offering opportunities to address the current reconfiguration of the everyday life of humans in the world of confinement

- to speculate about possible near futures after the Corona pandemic, starting in January 2021

The output expected from this project this week project is to have a living repository of solutions or ideas or creative ideas for people to implement other domestic spaces as well as to think about how a new productive society might be emerging from this current crisis of the Coronavirus, and how we can envision this productive society to finally emerge from 2020 as a transition year.

---

#### Below is a template of tasks for MDEF students, replace the dummy text with your designs and ideas 👇

# Protecting Masks made by #voluntaris3Dgarrotxa

#### Laura Freixas

**Introductory statement.**



## Document: DIY and emergent practices

Innova Didàctic (http://shop.innovadidactic.com/) is an educational robotics company that runs workshops for children and schools, sells electronic educational material and collaborates with the Robolot team (http://www.robolot.org/).

During this emergency situation, it is organizing the assembly of protective masks against covid-19 in the Olot area (Garrotxa). Innova Didàctic is in contact with more than 50 makers from La Garrotxa and 20 local companies that have 3D printers and supply of plastic screens.

In a week they have produced more than 3.600 protecting masks that have been distributed to different hospitals in Girona area, the workers of the Ajuntament d’Olot, homes for the elderly and other public services.

Some industrial companies like Noel, 3D Tecnics, GIPSA and EMOL had built a mold and they are injecting the frames of the mask. On Tuesday 30th March we received 600 injected masks at Innova Didàctic and the volunteers mounted the masks in two hours. Now the production is bigger, around 3.000 masks/day plus the ones made by the local makers.


You can find more information about the initiative in this article: https://www.naciodigital.cat/noticia/198958/robotica/garrotxa/es/bolca/fabricar/pantalles/protectores/amb/impressores/3d


---


## 1st Person Perspective

I will present you my experience with making protecting masks with the community #voluntaris3DGarrotxa.

![Example Image] https://gitlab.com/link-journal/link-x-mdef/_posts/_laura-freixas/_assets/1.png

There are more than 50 makers that are printing daily, then we have more than 20 companies that are printing and injecting masks with a mould and they produce more than 3.400masks/day.

Innova Didactic collects the makers production and some production of the local industries, and then the volunteers assemble all the masks with the plastic sheet. At the same time, they are printing some masks and develops with the team #robotsenys the oxygen masks.

They also receive calls from the sanitary and social sector and coordinates the deliveries with them. They have been supplying masks to hospitals in Olot, Girona and Barcelona.

![2] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/2.png

I’ve been three days volunteering for 10hours and the feelings they have transmitted to me are.

· Admiration: for the makers, there’s people that wake up at night two times to print more masks.

· Prevision: for the local industries because in two weeks they have developed an injection mold and they are mass producing masks.

· Adrenaline: when the sanitary people came at night to take the masks, they were very grateful and they gave us adrenaline with their words. When I arrived at night around 10pm I couldn’t sleep of the emotion.

· Passion: for me Innova Didàctic is an example of constant energy and love to their job. They’re working more han 12hours/day and are the nucleus of this volunteering system.

![3] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/3.png

![4] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/4.png



## Curate: collection of solutions, projects, creative ideas

Here you can see the design iterations of the protecting mask. They started with the Coronavirus Maker model but it takes a lot of time to print the cap visor, so they decided to redesign and simplify the mask.

And they did the first version with elastic band. Then they decided to try to laser cut metacrylate but it was to fragile and rigid.

So they decided to redesign the mask with elastic band and simplify it more. Finally from this design the local industries have made the injection model.

![5] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/5.png

Innova Didactic is also working with another community of volunteers in Montseny called Robotsenys, they are working with hospitals.

![6] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/6.png

This is the production space in Innova Didactic with some volunteers.

![7] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/7.png

Below some tweets related with the production of #voluntaris3Dgarrotxa.

![8] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/8.png

This picture was taken in the moment where sanitary people came to Innova Didactic to collect the masks at night.

![9] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/9.png

Here some tweets related with the use of the mask produced by the volunteers. As you can see there are different services using it: hospitals, local food shops, police, ambulances, residences…

![10] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/10.png

Since the 3rd of April they have developed a formulary to facilitate the online demand of masks.

![11] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/11.png

When Innova Didactic receives the masks from the makers they have a cleaning protocol: they clean it outside with water and bleach, they enter in a plastic bag, they take out the water, they perforate the plastic sheet, they assemble the frame with the plastic sheet, and they put it into cardboard boxes.

![12] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/12.png

There’s also a protocol made by Salut Department to clean the protecting masks during the use.

![13] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/13.png

Finally I’ve made a comparison between the protecting masks. The Coronavirus makers has a good relation: quality - price and is easy to distribute but has an uncomfortable restraint system.

The 3D print makers garrotxa has the same pros as the coronavirus makers but in the contras has a fragile structure.

The injected is a redesign of the makers garrotxa. Is comfortable, mass produced but in contras you need a big inversion of the mold and the injection machine.

The helmet is comfortable and made with resistant materials, but in the contras is more expensive and there aren’t enough per person.

![14] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/14.png

## Speculate: post-pandemic, envisioned transition

![15] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/16.png

![16] https://gitlab.com/link-journal/link-x-mdef/-/raw/master/_posts/_laura-freixas/_assets/16.png




![Example Image](https://images.unsplash.com/photo-1585348897755-b5b807bb642a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=668&q=80)


#### Questions & Feedback

[nhu.tram.veronica.tran@iaac.net](nhu.tram.veronica.tran@iaac.net)

[julia.danae.bertolaso@iaac.net](julia.danae.bertolaso@iaac.net)
